import express from 'express';

const app = express();

app.set('view engine', 'pug');

app.get('/', (req, res) => {
  res.render('pages/index');
});

app.get('/users/new', (req, res) => {
  res.render('users/new');
});

app.listen(3000, () => {
  console.log('App is running on 3000 port');
});
